module github.com/gogf/gf

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/clbanning/mxj v1.8.4
	github.com/fatih/structs v1.1.0
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gf-third/mysql v1.4.2
	github.com/gofrs/flock v0.7.1 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/websocket v1.4.1
	github.com/grokify/html-strip-tags-go v0.0.0-20190424092004-025bd760b278
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/theckman/go-flock v0.7.1
	golang.org/x/sys v0.0.0-20190910064555-bbd175535a8b // indirect
	golang.org/x/text v0.3.2
	google.golang.org/appengine v1.6.2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20190905181640-827449938966
)
